'use strict';

return module.exports = {
  set: require('./set'),
  map: require('./map'),
};

'use strict';
//TODO use get value for set as well as has

const getLeaf = require('./getLeaf');


if(module.parent) {
  module.exports = SetMap;
  return;
}

const sm = new SetMap();

const o1 = {foo:true, bar:false};
const o2 = {foo:true};
const o3 = {bar:false};
const o4 = {foo:true, bar:false, baz:'other'};

sm.set(o1, 1);
sm.set(o2, 2);
sm.set(o3, 3);

console.log(sm.has(o1), sm.has(o2), sm.has(o3), sm.has(o4));
console.log(sm.get({foo:true, bar:false}), sm.get({foo:true}), sm.get({bar:false}), sm.get({foo:true, bar:false, baz:'other'}));

/*
  if each key in
*/

function SetMap(keys) {
  if(this===global || this===null) {
    return new SetSet.apply(SetSet, arguments);
  }

  const fields = {};
  const setmap = new Map();

  this.set = set(this, fields, setmap);
  this.get = get(this, fields, setmap);
  this.has = has(this, fields, setmap);
  Object.freeze(this);
}





function set(self, fields, setmap) {
  return set;
  function set(obj, value) {

    let leaf = getLeaf(self, fields, obj);
    //console.log('setting', obj, leaf);
    if(leaf===null) {
      leaf = Object.assign({}, obj);

      for(let k in leaf) {

        if(!fields[k]) {
          fields[k] = new Map();
        }

        if(!fields[k].get(leaf[k])) {
          fields[k].set(leaf[k], new Set());
        }

        fields[k].get(leaf[k]).add(leaf);
      }
    }

    setmap.set(
      leaf,
      value
    );

    global.compare = leaf;
    //console.log('adding', obj, fields);

    return self;
  }
}

function get(self, fields, setmap) {
  return get;
  function get(obj) {
    let leaf = getLeaf(self, fields, obj);
    return setmap.get(leaf);
  }
}

function has(self, fields, setmap) {
  return has;
  function has(obj) {
    let leaf = getLeaf(self, fields, obj);
    //console.log('leaf:', obj, leaf);
    return Boolean(leaf);
  }
}

'use strict';

return module.exports = getLeaf;

function getLeaf(self, fields, obj) {
  //console.log('obj', obj);
  const keys = Object.keys(obj);
  if(keys.length===0) {
    throw Error(); // easier than dealing with it for now
  }
  if(Object.keys(fields).length===0) {
    return null;
  }
  
  
  const sets = [];
  for(let i=0; i<keys.length; ++i) {
    let key = keys[i];
    if(fields[key] && fields[key].has(obj[key])) {
      sets.push(fields[key].get(obj[key]));
    } else {
      return null;
    }
  }
  
  //console.log('sets:',sets);
  const possible = new Set();
  sets[0].forEach(function(item) {
    possible.add(item);
  });
  
  for(let i=1; i<sets.length; ++i) {
    possible.forEach(function(item) {
      if(!sets[i].has(item)) {
        possible.delete(item);
      }
    });
  }
  
  let found = null;
  possible.forEach(function(item) {
    if(Object.keys(item).length===keys.length) {
      if(found) {
        throw Error();
      } else {
        found = item;
      }
    }
  });
  
  return found;
}


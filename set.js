'use strict';

const getLeaf = require('./getLeaf');

if(module.parent) {
  module.exports = SetSet;
  return;
}

const ss = new SetSet();

const o1 = {foo:true, bar:false};
const o2 = {foo:true};
const o3 = {bar:false};
const o4 = {foo:true, bar:false, baz:'other'};

ss.add(o1);
ss.add(o2);
ss.add(o3);

console.log(ss.has(o1), ss.has(o2), ss.has(o3), ss.has(o4));
console.log(ss.has({foo:true, bar:false}), ss.has({foo:true}), ss.has({bar:false}), ss.has({foo:true, bar:false, baz:'other'}));

/*
  if each key in
*/

function SetSet(keys) {
  if(this===global || this===null) {
    return new SetSet.apply(SetSet, arguments);
  }

  const fields = {};

  this.add = add(this, fields);
  this.has = has(this, fields);
  Object.freeze(this);
}





function add(self, fields) {
  return add;
  function add(obj) {
    let leaf = getLeaf(self, fields, obj);
    //console.log('leaf:', obj, leaf);
    if(!leaf) {
      const newObj = Object.assign({}, obj);

      for(let k in newObj) {
        if(!fields[k]) {
          fields[k] = new Map();
        }

        if(!fields[k].get(newObj[k])) {
          fields[k].set(newObj[k], new Set());
        }

        fields[k].get(newObj[k]).add(newObj);
      }
    }
    //console.log('adding', obj, fields);
    return self;
  }
}


function has(self, fields) {
  return has;
  function has(obj) {
    let leaf = getLeaf(self, fields, obj);
    //console.log('leaf:', obj, leaf);
    return Boolean(leaf);
  }
}
